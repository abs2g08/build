#!/usr/bin/env bash
# prerequisites
## homebrew - http://brew.sh
## based on clean install of OSX 1.11.1 El Capitan

# TO-DO: create two branches for build repo 'home' and 'work'

brew update

# brew
brew install git
brew install git-flow

brew install node

sudo chown -R $(whoami) $(npm config get prefix)/{lib/node_modules,bin,share}

# npm
npm install -g mocha

npm install -g webpack
npm install -g grunt-cli
npm install -g jspm
npm install -g gulp

npm install -g n
npm install -g bower
npm install --global optipng-bin

npm install -g nodemon
npm install -g node-inspector

# TO-DO: add linter

# install cask
brew install caskroom/cask/brew-cask
brew cask update

echo 'export HOMEBREW_CASK_OPTS="--appdir=/Applications --caskroom=/opt/homebrew-cask/Caskroom/' >> ~/.bash_profile

# install software with cask
brew cask install caskroom/versions/java7
brew cask install google-chrome
brew cask install firefox
brew cask install google-drive
brew cask install google-hangouts
brew cask install sequel-pro
brew cask install sizeup #window mgmt for mac
brew cask install slack
brew cask install mou
brew cask install iterm2
brew cask install sourcetree #git GUI
brew cask install spotify
brew cask install sourcetree
brew cask install vlc

# atom
brew cask install atom
apm install eslint
apm install git-time-machine
apm install language-gitignore
apm install editorconfig
apm install ctrl-last-tab
apm install monokai-seti
apm install language-babel

yes | cp atom/style.less ./atom/style.less
yes | cp atom/keymap.cson ./atom/keymap.cson
yes | cp atom/config.cson ./atom/config.cson

# TO-DO: install core atom packages
# TO-DO: copy key bindings to atom config

# brew
brew install mcrypt php56-mcrypt
brew install mysql
brew install ant

# vagrant
brew cask install virtualbox
brew cask install vagrant
brew cask install vagrant-manager

# docker
brew cask install docker
brew cask install dockertoolbox

docker-machine create --driver "docker"
docker-machine start docker
eval "$(docker-machine env docker)"

# make folders
mkdir ~/test
mkdir ~/proj
mkdir ~/work
mkdir ~/tmp
mkdir ~/docs

cp -r tools ../tools

sudo gem update --system
sudo gem install -n /usr/local/bin compass

# stops .DS_Store saving on network drives
defaults write com.apple.desktopservices DSDontWriteNetworkStores true

# reindex spotlight
mdutil -i on /
mdutil -E /
